import React, { useEffect, useState, Fragment, useContext } from 'react'
import { useParams, Link, useNavigate } from 'react-router-dom'
import { Carousel } from 'react-bootstrap'
import UserContext from '../Store'
import Swal from 'sweetalert2';


const ProductDetail = () => {
    
    const {id} = useParams();
    const navigate = useNavigate();
    const [ prod, setProd ] = useState([]); 
    const {user, admin} = useContext(UserContext);
   
    const [quantity, setQuantity] = useState(1)
    
    const {_id, name, price} = prod;
    
    useEffect(() => {
        if(id){
            fetch(`${process.env.REACT_APP_API_URL}/api/v1/productDetail/${id}`)
            .then(result => result.json())
            .then(data => {
                //console.log(data)
                setProd(data);
                
            }) 
        } 
    }, [])

    //add order quantity
    const addQty = () => {
        const count = document.querySelector('.count')
        if (count.valueAsNumber >= prod.stocks) return;

        const qty = count.valueAsNumber + 1;
        setQuantity(qty)
     }
     const deductQty = () => {
        const count = document.querySelector('.count')
        if (count.valueAsNumber <= 1) return;

        const qty = count.valueAsNumber - 1;
        setQuantity(qty)
     }


     //ADDING ORDERS-------------------------------------------------------------------------------------

     const addToCart = () => {
        fetch(`${process.env.REACT_APP_API_URL}/api/v1/orderProduct/${id}`,{
            method: 'POST',
            headers:{
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                    name: name,
                    price: price,
                    quantity: quantity,
                    totalPrice: price * quantity
                })     
        })
        .then(result => result.json())
        .then(data => { 
            if(data){
                Swal.fire({
                    title: 'Product Ordered Successfully',
                    icon: "success",
                    text: `${data.name} Successfully Ordered!` 
                })
                navigate('/')  
            }    
     })
    }

  return (
    
    <Fragment>
        <div className="row f-flex justify-content-around">
            <div className="col-12 col-lg-5 img-fluid" id="product_image">
                <Carousel pause = 'hover'>
                    {/* {prod.images && prod.images.map(image =>(
                        <Carousel.Item key={image.public_id}>
                            <img className = "d-block w-100" src = {image.url} alt = {prod.title} />
                        </Carousel.Item>   
                    ))} */}
                </Carousel>    
            
            </div>

            <div className="col-12 col-lg-5 mt-5">
                <h3>{prod.name}</h3>
                <p id="product_id">Product #{prod._id}</p>

                <hr />

                <div className="rating-outer">
                    <div className="rating-inner" style={{width: `${(prod.rating / 5) * 100}%`}}></div>
                </div>
                <span id="no_of_reviews">({prod.numOfReviews} Reviews)</span>

                <hr />

                <p id="product_price"><i>&#8369;{prod.price}</i></p>
                <div className="stockCounter d-inline">
                    <span className="btn btn-danger minus" onClick={deductQty}>-</span>

                    <input type="number" className="form-control count d-inline" value={quantity} readOnly />

                    <span className="btn btn-primary plus" onClick={addQty}>+</span>
                </div>
                
                  {/* add to cart function insert here  */}
                  {
                    prod.stocks !== 0 && user && !admin? 
                    <button type="button" id="cart_btn" className="btn btn-primary d-inline ml-4" onClick={addToCart} >Order</button>  
                    : <Link type="button" id="cart_btn" className="btn btn-primary d-inline ml-4" disabled>Order</Link>
                  }              

                <hr />

                <p>Status: <span id="stock_status" className = {prod.stocks > 0 ? 'greenColor' : 'redColor'}>{prod.stocks > 0 ? 'In Stock' : "Out of Stock"}</span></p>

                <hr />

                <h4 className="mt-2">Description:</h4>
                <p>{prod.description}</p>
                <hr />
                
                <button id="review_btn" type="button" className="btn btn-primary mt-4" data-bs-toggle="modal" data-bs-target="#ratingModal">
                            Submit Your Review
                </button>
                
                <div className="row mt-2 mb-5">
                    <div className="rating w-50">

                        {/* bootstrap modal */}
                        <div className="modal fade" id="ratingModal" tabIndex="-1" role="dialog" aria-labelledby="ratingModalLabel" aria-hidden="true">
                            <div className="modal-dialog" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title" id="ratingModalLabel">Submit Review</h5>
                                        <button type="button" className="close" data-bs-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div className="modal-body">

                                        <ul className="stars" >
                                            <li className="star"><i className="fa fa-star"></i></li>
                                            <li className="star"><i className="fa fa-star"></i></li>
                                            <li className="star"><i className="fa fa-star"></i></li>
                                            <li className="star"><i className="fa fa-star"></i></li>
                                            <li className="star"><i className="fa fa-star"></i></li>
                                        </ul>

                                        <textarea name="review" id="review" className="form-control mt-3">

                                        </textarea>

                                        <button className="btn my-3 float-right review-btn px-4 text-white" data-bs-dismiss="modal" aria-label="Close">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                        
                </div>

            </div>
    </div>
    </Fragment>

  )
}

export default ProductDetail