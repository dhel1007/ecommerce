import {React, Fragment, useState} from 'react';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import MetaData from './MetaData';
import Sidebar from './Sidebar';

const AddProduct = () => {
 
    const navigate = useNavigate();    

    const [name, setName] = useState('')
    const [price, setPrice] = useState('')
    const [description, setDescription] = useState('')
    const [stocks, setStocks] = useState(0)

    const submitHandler = (e) => {
      e.preventDefault()

      fetch(`${process.env.REACT_APP_API_URL}/api/v1/admin/addProduct`, {
    method: "POST",
    headers: {
      'Content-Type' : 'application/json',
      Authorization: `Bearer ${localStorage.getItem('token')}`				
    },
    body: JSON.stringify({
      name: name,
      price: price,
      description: description,
      stocks: stocks
    })
  })
  .then(result => result.json())
  .then(data => {
    //console.log(data)
    if(data) {
      Swal.fire({
        title: "New Product Created!",
        icon: "success",
        text: " New Product Successfully Created!"
      })
      navigate("/")
    }
  })
}


  return (
    <Fragment>
      <MetaData title={'Adding Products'} />
      <div className='row'>
        <div className='col-12 col-md-2'>
          <Sidebar />
        </div>
        <div className='col-12 col-md-10'>
          <Fragment>
          <div className="wrapper my-5"> 
        <form className="shadow-lg" onSubmit={submitHandler
        }>
            <h1 className="mb-4">New Product</h1>

            <div className="form-group">
              <label htmlFor="name_field">Name</label>
              <input
                type="text"
                id="name_field"
                className="form-control"
                value={name}
                onChange ={e => setName(e.target.value)}
              />
            </div>

            <div className="form-group">
                <label htmlFor="price_field">Price</label>
                <input
                  type="text"
                  id="price_field"
                  className="form-control"
                  value={price}
                onChange ={e => setPrice(e.target.value)}
                />
              </div>

              <div className="form-group">
                <label htmlFor="description_field">Description</label>
                <textarea className="form-control" 
                id="description_field" 
                rows="8" 
                value={description}
                onChange ={e => setDescription(e.target.value)}
                ></textarea>
              </div>

              <div className="form-group">
                <label htmlFor="stock_field">Stock</label>
                <input
                  type="number"
                  id="stock_field"
                  className="form-control"
                  value={stocks}
                  onChange ={e => setStocks(e.target.value)}
                />
              </div>
  
            <button
              id="login_button"
              type="submit"
              className="btn btn-block py-3"
            >
              CREATE
            </button>

          </form>
          </div>
          </Fragment>
        </div>
      </div>
    </Fragment>
  )
}

export default AddProduct