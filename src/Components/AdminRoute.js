import { Navigate } from "react-router-dom";
import UserContext from "../Store";
import { useContext } from "react";

const Protected = ({ children }) => {

  const {admin} = useContext(UserContext)

  if (!admin) {
    return <Navigate to="/" replace />;
  }
  return children;
};
export default Protected;
