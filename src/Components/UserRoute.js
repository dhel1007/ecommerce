
import { Navigate } from "react-router-dom";
import UserContext from "../Store";
import { useContext, React } from "react";
const UserRoute = ({ children }) => {

    const {user, admin} = useContext(UserContext)
    
    if (!user) {
        return <Navigate to="/" replace />;
      }
    else if(admin){
        return <Navigate to="/" replace />;
    }
      return children;
  
}

export default UserRoute