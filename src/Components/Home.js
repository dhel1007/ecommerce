import {React, useEffect, useState,Fragment} from 'react'
import Product from './Product'
import MetaData from './MetaData'

const Home = () => {

  const [allProduct, setAllProduct] = useState([])
  
    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/api/v1/allProducts`)
        .then(result => result.json())
        .then(data => {
           //console.log(data)
            setAllProduct(data.map(product => {
              return (
                <Product key={product._id} productProp = { product } /> 
              )
            }))
        })
    }, [])
    
  return (
    <Fragment>
			<MetaData title = {"Best Product Online"}/>
        
        <h1 id="products_heading">Latest Products</h1>

        <section id="products" className="container mt-5">
            <div className="row">
                {allProduct}
            </div>
        </section>
		</Fragment>
  )
}

export default Home