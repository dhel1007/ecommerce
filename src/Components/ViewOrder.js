import { React, useEffect, useState, Fragment }from 'react'
import { MDBDataTable } from 'mdbreact';
import MetaData from './MetaData';

const ViewOrder = () => {

    const [order, setOrder] = useState([]);
   
    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/api/v1/viewOrders`,{
          headers:{
            Authorization: `Bearer ${localStorage.getItem('token')}`
            }
         })
         .then(result => result.json())
         .then(data => {
            setOrder(data.map(orders => {
                return orders
            }))
         })
        
    }, [])
    const setProducts = () => {
        const datas = {
          columns: [
            {
              label: 'ID',
              field: 'id',
              sort: 'asc'
            },
            {
              label: 'Name',
              field: 'name',
              sort: 'asc'
            },
            {
              label: 'Price',
              field: 'price',
              sort: 'asc'
            },
            {
              label: 'Quantity',
              field: 'quantity',
              sort: 'asc'
            },
            {
              label: 'Total Amount',
              field: 'total_amount'
            },
          ],
          rows: []
        }
        order.forEach(product => {
          datas.rows.push({
            id: product._id,
            name: product.name,
            price: product.price,
            quantity: product.quantity, 
            total_amount: product.totalPrice          
            
          })
        })
        return datas;
      
      }
  return (
    <Fragment>
      <MetaData title={'My Orders'} />
      <div className='row'>
        <div className='col-12 col-md-10'>
          <Fragment>
            <h1 className ="my-5">My Orders</h1>
              <MDBDataTable 
                data = {setProducts()}
                className = "px-3"
                bordered
                striped
                hover
              />
              
          </Fragment>
        </div>
      </div>
    </Fragment>
  )
}

export default ViewOrder