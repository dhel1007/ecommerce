import {React, Fragment, useEffect, useContext} from 'react'
import {Link} from 'react-router-dom'
import {Nav, Navbar, Form, Container, Button} from 'react-bootstrap'
import UserContext from '../Store'

const AppNavbar = () => {

const {user} = useContext(UserContext)
const {admin} = useContext(UserContext)

useEffect(() => {

}, [])

  return (

    <Fragment>
        <Navbar bg="light" expand="lg">
      <Container fluid>
        <Navbar.Brand href ="/">LazShop</Navbar.Brand>
        <Navbar.Toggle aria-controls="navbarScroll" />
        <Navbar.Collapse id="navbarScroll">
          <Nav
            className="mx-auto my-2 my-lg-0 ml-4"
            style={{ maxHeight: '100px' }}
            navbarScroll
          >
          <Link to="/" className='mx-5'>Home</Link>  

          {
            admin ? <Link to="/dashboard" className='mx-5'>Dashboard</Link> :
            ''
          } 
          
          {
            user ? 
            <Fragment>
              <Link to="/orders/me" className='mx-5'>My Orders</Link>
              <Link to="/me" className='mx-5'>My Profile</Link>
              <Link to = '/logout' className='mx-5'>Logout</Link>
               
            </Fragment>
            
            : 
            <Fragment>
              <Link to="/login" className='mx-5'>Login</Link>
              <Link to="/register" className='mx-5'>Register Now!</Link>
            </Fragment>  
          }
             
          </Nav>
          <Form className="d-flex">
            <Form.Control
              type="search"
              placeholder="Enter Product Name"
              className="me-2"
              aria-label="Search"
            />
            <Button variant="outline-success">Search</Button>
          </Form>
        </Navbar.Collapse>
      </Container>
    </Navbar> 
    </Fragment>
  )
}

export default AppNavbar