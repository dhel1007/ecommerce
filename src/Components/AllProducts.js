import { React, useEffect, Fragment, useState } from 'react'
import { MDBDataTable } from 'mdbreact';
import { Link } from 'react-router-dom';

import MetaData from './MetaData';
import Sidebar from './Sidebar.js'


const AllProducts = () => {
  
  const [prod, setProd] = useState([]);
   
  

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/api/v1/admin/allProduct`,{
      headers:{
        Authorization: `Bearer ${localStorage.getItem('token')}`
    }
    })
    .then(result => result.json())
    .then(data => {
       //console.log(data)
       setProd(data.map(product => {
        return product;
       }))
    })
  }, [prod])

 
  const archiveProduct = (id) => {

      fetch(`${process.env.REACT_APP_API_URL}/api/v1/admin/archiveProducts/${id}`, {
        method: 'PUT',
        headers:{
          'Content-Type' : 'application/json',
          Authorization: `Bearer ${localStorage.getItem('token')}`
      } 
      }) 
  }

const setProducts = () => {
  const datas = {
    columns: [
      {
        label: 'ID',
        field: 'id',
        sort: 'asc'
      },
      {
        label: 'Name',
        field: 'name',
        sort: 'asc'
      },
      {
        label: 'Price',
        field: 'price',
        sort: 'asc'
      },
      {
        label: 'Stocks',
        field: 'stocks',
        sort: 'asc'
      },
      {
        label: 'Status',
        field: 'status'
      },
      {
        label: 'Actions',
        field: 'actions'
      },
    ],
    rows: []
  }
  prod.forEach(product => {
    datas.rows.push({
      id: product._id,
      name: product.name,
      price: product.price,
      stocks: product.stocks,
      status: String(product.isActive),
      actions: <Fragment>
          <Link to={`/admin/updateProduct/${product._id}`} className="btn btn-primary py-1 px-2"><i className='fa fa-eye'></i>
          <i className='fa fa-pencil'></i>
          </Link>
          {
            product.isActive ?
          <button className='btn btn-danger py-1 px-2 ml-2'>
            <i className='fa fa-trash' onClick={() => archiveProduct(product._id)}></i>
          </button>
          :
          <button className='btn btn-success py-1 px-2 ml-2'>
            <i className='fa fa-trash' onClick={() => archiveProduct(product._id)}></i>
          </button>
          }
         
        </Fragment>
      
    })
  })
  return datas;

}
  return (
    <Fragment>
      <MetaData title={'All Products'} />
      <div className='row'>
        <div className='col-12 col-md-2'>
          <Sidebar />
        </div>
        <div className='col-12 col-md-10'>
          <Fragment>
            <h1 className ="my-5">All Products</h1>
              <MDBDataTable 
                data = {setProducts()}
                className = "px-3"
                bordered
                striped
                hover
              />
              
          </Fragment>
        </div>
      </div>
    </Fragment>
  )
}

export default AllProducts