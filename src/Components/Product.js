import React from 'react'
import { Link } from 'react-router-dom'
import ProductDetail from './ProductDetail'

const Product = ({productProp}) => {

    const { _id, name, price, ratings, numOfReviews } = productProp

  return (
    <div className="col-sm-12 col-md-6 col-lg-3 my-3">
        <div className="card p-3 rounded">
            <img
            className="card-img-top mx-auto"
            //src={product.images[0].url}
            alt = 'product'
            />
            <div className="card-body d-flex flex-column">
                <h5 className="card-title">
                    <Link to = {`/product/${_id}`} >{name}</Link>
                </h5>
                <div className="ratings mt-auto">
                    <div className="rating-outer">
                    <div className="rating-inner" style={{width: `${(ratings / 5) * 100}%`}}></div>
                    </div>
                    <span id="no_of_reviews">({numOfReviews} Reviews)</span>
                </div>
                <p className="card-text">{price}</p>
                <Link to={`/product/${_id}`} id="view_btn" className="btn btn-block" onClick={ProductDetail()}>View Details</Link>
            </div>
        </div>
        
    </div>
  )
}

export default Product