import { React, useEffect, useState, Fragment } from 'react'
import { useNavigate } from 'react-router-dom'
import MetaData from './MetaData'
import Swal from 'sweetalert2'

const Register = () => {

    const navigate = useNavigate()

    const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")
	const [confirmPassword, setConfirmPassword] = useState("")
	const [firstName, setFirstName] = useState("")
	const [lastName, setLastName] = useState("")

    const[isActive, setIsActive] = useState(false)

    useEffect(() => {
		if(email !== "" && password !== "" && confirmPassword!== "" && password === confirmPassword){
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	}, [email, password, confirmPassword])

    const submitHandler = (e) => {
        e.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/api/v1/register`, {
			method: "POST",
			headers: {
				'Content-Type' : 'application/json'				
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				password: password
			})
		})
        .then(result => result.json())
        .then(data => {

            if(data){
                Swal.fire({
					title: "Account Created!",
					icon: "success",
					text: " Welcome to our Website!"
				})
                navigate("/login")
            }else{
                Swal.fire({
					title: "ERROR!",
					icon: "error",
					text: `Email already Taken!`
				})

				navigate("/register")
            }
        })
    }

  return (
    <Fragment>
        <MetaData title={ "Register User" } />
            <div className="row wrapper">
                <div className="col-10 col-lg-5">
                    <form className="shadow-lg" onSubmit={submitHandler} encType='multipart/form-data'>
                        <h1 className="mb-3">Register</h1>

                    <div className="form-group">
                        <label htmlFor="email_field">First Name</label>
                        <input 
                        type="name" 
                        id="name_field1" 
                        className="form-control" 
                        name = 'firstName'
                        value={firstName}
                        onChange = {e =>setFirstName(e.target.value)}
                         />
                    </div>
                    <div className="form-group">
                        <label htmlFor="email_field">Last Name</label>
                        <input 
                        type="name" 
                        id="name_field2" 
                        className="form-control" 
                        name = 'lastName'
                        value={lastName}
                        onChange = {e =>setLastName(e.target.value)} 

                        />
                    </div>

                        <div className="form-group">
                        <label htmlFor="email_field">Email</label>
                        <input
                            type="email"
                            id="email_field"
                            className="form-control"
                            name = 'email'
                            value={email}
                            onChange = {e =>setEmail(e.target.value)}
                        />
                        </div>
            
                        <div className="form-group">
                        <label htmlFor="password_field">Password</label>
                        <input
                            type="password"
                            id="password_field"
                            className="form-control"
                            name = 'password'
                            value={password}
                            onChange = {e =>setPassword(e.target.value)}
                        />
                        </div>

                        <div className="form-group">
                        <label htmlFor="email_field3">Confirm Password</label>
                        <input
                            type="password"
                            id="email_field3"
                            className="form-control"
                            name = 'email'
                            value={confirmPassword}
                            onChange = {e =>setConfirmPassword(e.target.value)}
                        />
                        </div>

                    {/* <div className='form-group'>
                        <label htmlFor='avatar_upload'>Avatar</label>
                        <div className='d-flex align-items-center'>
                            <div>
                                <figure className='avatar mr-3 item-rtl'>
                                    <img
                                        src={avatarPreview}
                                        className='rounded-circle'
                                        alt='Avatar Preview'
                                    />
                                </figure>
                            </div>
                            <div className='custom-file'>
                                <input
                                    type='file'
                                    name='avatar'
                                    className='custom-file-input'
                                    id='customFile'
                                    accept='images/*'
                                    onChange={onChange}
                                />
                                <label className='custom-file-label' htmlFor='customFile'>
                                    Choose Avatar
                                </label>
                            </div>
                        </div>
                    </div>  */}
            
                        <button
                        id="register_button"
                        type="submit"
                        className="btn btn-block py-3"
                        disabled={ !isActive }
                        >
                        REGISTER
                        </button>
                    </form>
                </div>
            </div>
    </Fragment>

  )
}

export default Register