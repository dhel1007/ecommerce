import {Fragment, useContext, useEffect, useState} from 'react';
import { Link, useNavigate } from 'react-router-dom';
import MetaData from './MetaData';

import Swal from 'sweetalert2';
import UserContext from '../Store';


const Login = () => {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const navigate = useNavigate();

    const { setUser } = useContext(UserContext);

    
  const submitHandler = (e) => {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/api/v1/login`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            email: email,
            password: password
        })
    })
    .then(result => result.json())
    .then(data => {
        //console.log(data)
      if(data === false){
            Swal.fire({
                title: "Error!",
                icon: "error",
                text: "Email or Password is in Incorrect!"
            })
        }else{
            localStorage.setItem('token', data.auth)
            retrieveUserDetails(localStorage.getItem('token'))

            Swal.fire({
                title: "Authentication Successful!",
                icon: "success",
                text: `Welcome !, Happy Shopping!` 
            })
            navigate('/')  

        }
    })
   
   const retrieveUserDetails = (token) => {
    fetch(`${process.env.REACT_APP_API_URL}/api/v1/userDetails`,{
        headers:{
            Authorization: `Bearer ${token}`
        }
    })
    .then(result => result.json())
    .then(data => { 
        
        setUser(
                {
                    id: data._id,
                    isAdmin: data.isAdmin
                }
            )
    })

 }
}

  return (
    <Fragment>
    <MetaData title={"Login Page"} />
    <Fragment>

        <div className="row wrapper"> 
            <div className="col-10 col-lg-5">
                <form className="shadow-lg" onSubmit={submitHandler}>
                    <h1 className="mb-3">Login</h1>
                    <div className="form-group">
                    <label htmlFor="email_field">Email</label>
                    <input
                        type="email"
                        id="email_field"
                        className="form-control"
                        value={email}
                        onChange = {(e) => setEmail(e.target.value)}
                    />
                    </div>
        
                    <div className="form-group">
                    <label htmlFor="password_field">Password</label>
                    <input
                        type="password"
                        id="password_field"
                        className="form-control"
                        value = {password}
                        onChange = {(e) => setPassword(e.target.value)}
                    />
                    </div>

                    <a href="/forgetpassword" className="float-right mt-2">Forgot Password?</a>
        
                    <div>
                        <button 
                        id="login_button"
                        type="submit"
                        className="btn btn-block py-2"
                        >
                        LOGIN
                        </button>
                    </div>

                    <Link to="/register" className="float-right mt-3">Sign up here</Link>
                    

                    
                </form>
            </div>
        </div>


    </Fragment>
</Fragment>
  )
}

export default Login;