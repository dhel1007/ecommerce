import {React, useContext, useEffect} from 'react'
import { Navigate } from 'react-router-dom'
import UserContext from '../Store'
import Swal from 'sweetalert2'

const Logout = () => {

    const { setUser, unSetUser, setAdmin} = useContext(UserContext)
    
    useEffect(() => {
        unSetUser()
        setUser(null)
        setAdmin(null)

        Swal.fire({
            title: "Log Out!",
			      icon: "success"
        })

    }, [])

  return (
    <Navigate to = '/login' />
  )
}

export default Logout