import './App.css';
import { useState, useEffect } from 'react';
import { BrowserRouter as Router, Routes, Route} from 'react-router-dom';

import AppNavbar from "./Components/Navbar"
import Login from './Components/login';
import Logout from './Components/Logout';
import Home from './Components/Home'
import ProductDetail from './Components/ProductDetail';
import Register from './Components/Register';
import { UserProvider } from './Store';
import ViewOrder from './Components/ViewOrder';
import Dashboard from './Components/Dashboard';
import AddProduct from './Components/AddProduct';
import AllProducts from './Components/AllProducts';
import Protected from './Components/AdminRoute';
import UserRoute from './Components/UserRoute';
import UpdateProduct from './Components/UpdateProduct';



function App() {
  const [user, setUser] = useState(null)
  const [admin, setAdmin] = useState(null)
 

  const unSetUser = () => {
    localStorage.clear()
  }

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/api/v1/userDetails`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(result => result.json())
    .then(data => {
     // console.log(data)
      if(localStorage.getItem('token') !== null){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
        setAdmin(data.isAdmin)
      }else{
        setUser(null)
      }
    })

  }, [])
  useEffect(() => {

  }, [user, admin])
  

  
  return (
    <UserProvider value={{user, setUser, unSetUser, admin, setAdmin}} >
    <Router>
      <AppNavbar />
      <Routes>
        <Route path = "/" element = {<Home/>}/>
        <Route path = "/login" element = {<Login/>}/>
        <Route path = '/product/:id' element = {<ProductDetail/>}/>
        <Route path = "/register" element = {<Register/>}/>
        <Route path = "/logout" element = {<Logout/>}/>


        {/* <Route path = "/cart" element = {<Cart/>}/> */}

        <Route path = "/orders/me" element = {
          <UserRoute >
              <ViewOrder />
          </UserRoute>
        } />

        <Route path = "/dashboard" element = {
          <Protected >
              <Dashboard />
          </Protected>
        } />
        <Route path = "/admin/products" element = {
          <Protected >
              <AllProducts/>
          </Protected>
        } />
        <Route path = "/admin/addProduct" element = {
          <Protected >
               <AddProduct/>
          </Protected>
        } />   

        <Route path = "/admin/updateProduct/:id" element = {
          <Protected >
               <UpdateProduct/>
          </Protected>
        } /> 
             
      </Routes>
    </Router>
   </UserProvider>
  
    
  );
}

export default App;
